﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Snake
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Point> llistaPomes = new List<Point>();
        Brush snakeColor = Brushes.Yellow;
        ImageBrush poma = new ImageBrush();
        enum DIRECCIO
        {
            NORD = 8,
            SUD = 2,
            OEST = 4,
            EST = 6
        };
        Point puntInici = new Point(250,250);
        Point posicioActual = new Point();
        int direccio = 0;
        int direccioPrevia = 0;
        int tamanySnake = 20;
        int llargada = 1;
        int punts = 0;
        Random rnd = new Random();

        public MainWindow()
        {
            InitializeComponent();
            poma.ImageSource = new BitmapImage(new Uri("poma.png", UriKind.Relative));
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            TimeSpan velocitat = new TimeSpan(500000);
            timer.Interval = velocitat;
            timer.Start();

            this.KeyDown += new KeyEventHandler(OnButtonKeyDown);
            pintaSnake(puntInici);
            posicioActual = puntInici;

            // Pinta les pomes
            for (int n = 0; n < 5; n++)
            {
                pintaPomes(n);
            }
        }
        private void pintaSnake(Point posicio)
        {

            Ellipse serp = new Ellipse();
            serp.Fill = snakeColor;
            serp.Width = tamanySnake;
            serp.Height = tamanySnake;

            Canvas.SetTop(serp, posicio.Y);
            Canvas.SetLeft(serp, posicio.X);

            paintCanvas.Children.Add(serp);

            if (paintCanvas.Children.Count - llistaPomes.Count > llargada)
            {
                paintCanvas.Children.RemoveAt(paintCanvas.Children.Count - (llargada+1));
            }
        }
        private void pintaPomes(int index)
        {
            Point bonusPoint = new Point(rnd.Next(0, 25)*tamanySnake, rnd.Next(0, 25)*tamanySnake);

            Ellipse pomes = new Ellipse();
            pomes.Fill = poma;
            pomes.Width = tamanySnake;
            pomes.Height = tamanySnake;

            Canvas.SetTop(pomes, bonusPoint.Y);
            Canvas.SetLeft(pomes, bonusPoint.X);
            paintCanvas.Children.Insert(index, pomes);
            llistaPomes.Insert(index, bonusPoint);
        }
        private void OnButtonKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Down:
                    if (direccioPrevia != (int)DIRECCIO.NORD)
                        direccio = (int)DIRECCIO.SUD;
                    break;
                case Key.Up:
                    if (direccioPrevia != (int)DIRECCIO.SUD)
                        direccio = (int)DIRECCIO.NORD;
                    break;
                case Key.Left:
                    if (direccioPrevia != (int)DIRECCIO.EST)
                        direccio = (int)DIRECCIO.OEST;
                    break;
                case Key.Right:
                    if (direccioPrevia != (int)DIRECCIO.OEST)
                        direccio = (int)DIRECCIO.EST;
                    break;
            }
            direccioPrevia = direccio;
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            // Determina que la snake torni pel costat contrari al que ha sortit.
            if (posicioActual.X < 10 && direccio == (int)DIRECCIO.OEST)
                posicioActual.X = 490;
            else if (posicioActual.X > 470 && direccio == (int)DIRECCIO.EST)
                posicioActual.X = -10;
            else if (posicioActual.Y < 10 && direccio == (int)DIRECCIO.NORD)
                posicioActual.Y = 490;
            else if (posicioActual.Y > 470 && direccio == (int)DIRECCIO.SUD)
                posicioActual.Y = -10;

            // Mou el cap de la serp en la direccio del moviment.
            switch (direccio)
            {
                case (int)DIRECCIO.SUD:
                    posicioActual.Y += 10;
                    pintaSnake(posicioActual);
                    break;
                case (int)DIRECCIO.NORD:
                    posicioActual.Y -= 10;
                    pintaSnake(posicioActual);
                    break;
                case (int)DIRECCIO.OEST:
                    posicioActual.X -= 10;
                    pintaSnake(posicioActual);
                    break;
                case (int)DIRECCIO.EST:
                    posicioActual.X += 10;
                    pintaSnake(posicioActual);
                    break;
            }
            // Quan l'snake agafa una poma creix.
            int n = 0;
            foreach (Point point in llistaPomes)
            {

                if ((Math.Abs(point.X - posicioActual.X) < tamanySnake) &&
                    (Math.Abs(point.Y - posicioActual.Y) < tamanySnake))
                {
                    llargada += 1;
                    punts += 10;

                    // Elimina les pomes que has agafat.
                    llistaPomes.RemoveAt(n);
                    paintCanvas.Children.RemoveAt(n);
                    if (llistaPomes.Count == 0) //Si es la ultima poma, guanyes.
                        GameOver();
                    break;
                }
                n++;
            }
        }
        private void GameOver()
        {
            MessageBox.Show("Has guanyat!");
            this.Close();
        }
    }
}
